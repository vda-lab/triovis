class Row{
	Variant parentVariant;
	int coverage;
	int varfreq;
	int sampleIndex;
    boolean isRefRef, isRefAlt, isAltAlt;
	//constructor
	Row (Variant variant, int totalCount, int varf, int sIndex){
        parentVariant = variant;
        coverage = totalCount;
        varfreq = varf;
        sampleIndex = sIndex;
        // isValidCoverage = true; 
    }

    String toString(){
    	return "c:"+coverage+"(v:"+varfreq+")";
    }
}
