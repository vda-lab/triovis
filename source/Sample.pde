class Sample{
	String sampleID;
	HashMap<Integer, ArrayList<Row>> rowCoverageMap; //coverage and Arraylist
    HashMap<Integer, ArrayList<Row>> rowVarfreqMap; //Varfreq and ArrayList
    ArrayList<Row> rows; //all the rows 

    //Distribution
    Distribution varfreqDistribution, coverageDistribution;

    //coverage
    int currentCoverage = 1;
    //varfreq
    int currentVarfreqLow = 20;
    int currentVarfreqHigh = 90;
    int maxVarfreq = 100;

	//constructor
	Sample(String id){
		sampleID = id;
		rowCoverageMap = new HashMap<Integer, ArrayList<Row>>();
        rowVarfreqMap = new HashMap<Integer, ArrayList<Row>>();  
        rows = new ArrayList<Row>();
	}


	void addRow(Row row) {
        int coverage = row.coverage;
        int varfreq = row.varfreq;
        //coverage 
        ArrayList<Row> cArray = rowCoverageMap.get(coverage);
        if(cArray == null){
            cArray = new ArrayList<Row>();
            rowCoverageMap.put(coverage, cArray);
        }
        cArray.add(row);
        //varfreq
        ArrayList<Row> vArray = rowVarfreqMap.get(varfreq);
        if(vArray == null){
            vArray = new ArrayList<Row>();
            rowVarfreqMap.put(varfreq, vArray);
        }
        vArray.add(row);
        //add to general arraylist
        rows.add(row);
    }

    //called only once at the beggining
    void setupZygosity() {
        // println("debug:Sample.setupZygocity():"+sampleID);
        int discardedCounter = 0;
        for(int i = 0; i< rows.size(); i++){
            Row r = rows.get(i);
            int varfreq = r.varfreq;
            if(varfreq <= maxVarfreq && varfreq >= currentVarfreqHigh){
                //homozygous
                r.isRefRef = false;
                r.isRefAlt = false;
                r.isAltAlt = true;
            }else if( varfreq < currentVarfreqHigh && varfreq >= currentVarfreqLow){
                //hetrozygous
                r.isRefRef = false;
                r.isRefAlt = true;
                r.isAltAlt = false;
            }else{
                // println("Debug:discard:"+sampleID+":"+r);
                r.isRefRef = false;
                r.isRefAlt = false;
                r.isAltAlt = false;
                discardedCounter++;
            }
        }
        println("debug:"+sampleID+" has "+discardedCounter+" rows with varfreq below 20");
    }
    //for a set of Row object, update zygocity (slider interaction)
    void updateZygosity(ArrayList<Row> rowsToUpdate) {
        if(rowsToUpdate != null){
            for(int i = 0; i< rowsToUpdate.size(); i++){
                Row r = rowsToUpdate.get(i);
                int varfreq = r.varfreq;
                if(varfreq <= maxVarfreq && varfreq >= currentVarfreqHigh){
                    //homozygous
                    r.isRefRef = false;
                    r.isRefAlt = false;
                    r.isAltAlt = true;
                }else if( varfreq <currentVarfreqHigh && varfreq >= currentVarfreqLow){
                    //hetrozygous
                    r.isRefRef = false;
                    r.isRefAlt = true;
                    r.isAltAlt = false;
                }else{
                    r.isRefRef = false;
                    r.isRefAlt = false;
                    r.isAltAlt = false;
                }
            }
        }
    }

    //get Rows from the spacified coverage range 
    ArrayList<Row>[] getRows(int cMin, int cMax) {
        int bins = cMax - cMin;
        ArrayList<Row>[] result = (ArrayList<Row>[])new ArrayList[bins];
        
        for(int i = cMin; i<cMax; i++){
            ArrayList<Row> selectedRows = new ArrayList<Row>();
            ArrayList<Row> target = rowCoverageMap.get(i);
            if(target != null){
                selectedRows.addAll(target);
                result[i-1] = selectedRows;
            }
        }
        return result;
    }

    //update varfreq range and return arraylist of Row
    ArrayList<Variant> updateVarfreqRange(int filterMin, int filterMax) {
        ArrayList<Variant> variantsToUpdate = new ArrayList<Variant>();
        int prevLow = currentVarfreqLow;
        int prevHigh = currentVarfreqHigh;
        currentVarfreqLow = filterMin;
        currentVarfreqHigh = filterMax;
        
        //low end
        if(prevLow > currentVarfreqLow){
            //decreased
            for(int i = currentVarfreqLow; i <prevLow; i++){
                ArrayList<Row> rowsToUpdate = rowVarfreqMap.get(i);
                updateZygosity(rowsToUpdate);
                variantsToUpdate.addAll(getVariants(rowsToUpdate));
            }
        }else if(prevLow < currentVarfreqLow){
            //increased
            for(int i = prevLow; i <currentVarfreqLow; i++){
                ArrayList<Row> rowsToUpdate = rowVarfreqMap.get(i);
                updateZygosity(rowsToUpdate);
                variantsToUpdate.addAll(getVariants(rowsToUpdate));
            }
        }
        
        //high end
        if(prevHigh > currentVarfreqHigh){
            //decreased
            for(int i = currentVarfreqHigh; i<prevHigh; i++){
                ArrayList<Row> rowsToUpdate = rowVarfreqMap.get(i);
                updateZygosity(rowsToUpdate);
                variantsToUpdate.addAll(getVariants(rowsToUpdate));
            }
        }else if( prevHigh < currentVarfreqHigh){
            for(int i = prevHigh; i<currentVarfreqHigh; i++){
                ArrayList<Row> rowsToUpdate = rowVarfreqMap.get(i);
                updateZygosity(rowsToUpdate);
                variantsToUpdate.addAll(getVariants(rowsToUpdate));
            }
        }
        return variantsToUpdate;
    }

    //get all the variants from the rows
    ArrayList<Variant> getVariants(ArrayList<Row> rowsToUpdate) {
        if(rowsToUpdate != null){
            ArrayList<Variant> result = new ArrayList<Variant>();
            for(int i = 0; i< rowsToUpdate.size(); i++){
                Row r = rowsToUpdate.get(i);
                result.add(r.parentVariant);
            }
            return result;
        }
        return new ArrayList<Variant>();
    }

    //update minimum coverage
    //select and return vairants to update
    ArrayList<Variant> updateCoverage(int filterMin, int prevCoverage) {
        //find all rows
        ArrayList<Row> rowsToUpdate = new ArrayList<Row>();
        if(filterMin > prevCoverage){
            //increasing coverage
            println("--increasing coverage: from "+prevCoverage+" to "+filterMin);
            for(int i = prevCoverage; i < filterMin; i++){
                ArrayList<Row> rows = rowCoverageMap.get(i);
                if(rows != null){ //some may have no Row at higher coverage
                    rowsToUpdate.addAll(rows);
                }
            }
        }else if(filterMin < prevCoverage){
            //decreasing coverage
            println("--decreasing coverage: from "+prevCoverage+" to "+filterMin);
            for(int i = filterMin; i< prevCoverage; i++){
                ArrayList<Row> rows = rowCoverageMap.get(i);
                if(rows != null){ //some may have no Row at higher coverage
                    rowsToUpdate.addAll(rows);
                }
            }
        }else{
            //System.out.println("no coverage change.... updateCoverage()");
        }
        
        //iterate through rows and find affected variants
        ArrayList<Variant> variantsToUpdate = new ArrayList<Variant>();
        for(int i = 0; i < rowsToUpdate.size(); i++){
            Row r = rowsToUpdate.get(i);
            Variant v = r.parentVariant;
            variantsToUpdate.add(v);
            // println("\t"+v.variantID+":"+v.rows);
        }
        
        return variantsToUpdate; 
    }

    ConcurrentHashMap<String, Variant> updateCoverage2(int filterMin, int prevCoverage) {
        //find all rows
        ArrayList<Row> rowsToUpdate = new ArrayList<Row>();
        if(filterMin > prevCoverage){
            //increasing coverage
            // println("--increasing coverage: from "+prevCoverage+" to "+filterMin);
            for(int i = prevCoverage; i < filterMin; i++){
                ArrayList<Row> rows = rowCoverageMap.get(i);
                if(rows != null){ //some may have no Row at higher coverage
                    rowsToUpdate.addAll(rows);
                }
            }
        }else if(filterMin < prevCoverage){
            //decreasing coverage
            // println("--decreasing coverage: from "+prevCoverage+" to "+filterMin);
            for(int i = filterMin; i< prevCoverage; i++){
                ArrayList<Row> rows = rowCoverageMap.get(i);
                if(rows != null){ //some may have no Row at higher coverage
                    rowsToUpdate.addAll(rows);
                }
            }
        }else{
            //System.out.println("no coverage change.... updateCoverage()");
        }
        
        //iterate through rows and find affected variants
        ConcurrentHashMap<String, Variant> variantsToUpdate = new  ConcurrentHashMap<String, Variant>();
        for(int i = 0; i < rowsToUpdate.size(); i++){
            Row r = rowsToUpdate.get(i);
            Variant v = r.parentVariant;
            variantsToUpdate.put(v.chrIndex+"_"+v.position, v);
            // println("\t"+v.variantID+":"+v.rows);
        }
        
        return variantsToUpdate; 
    }


}
