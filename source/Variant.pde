class Variant{
	int chrIndex;
	int position;
	ArrayList<Row> rows;
	String variantID;
    int[] cellIndex;


	//constructor
	Variant(int chr, int position, String id){
        this.position = position;
        chrIndex = chr;
        rows = new ArrayList<Row>();
        if(id.equals(".")){
            variantID = null;
        }else{
            variantID = id;
        }
    }

    void addRow(Row r){    
        rows.add(r);
    }

    Row getRow(int sampleIndex){
        for(int i = 0; i<rows.size(); i++){
            Row r = rows.get(i);
            if(r.sampleIndex == sampleIndex){
                return r;
            }
        }
        return null;
    }
}
